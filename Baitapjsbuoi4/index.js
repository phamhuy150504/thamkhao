/** bài 1
 * input người dùng nhập vào 3 số nguyên
 * progress lấy value and ss use if else 
 * output sắp xếp theo thứ tự tăng dần
 */

 document.getElementById('b1-btn').addEventListener('click', function() {
    var so1 = document.getElementById('so1').value*1;
    var so2 = document.getElementById('so2').value*1;
    var so3 = document.getElementById('so3').value*1;
    if (so1 > so2 && so1 > so3) {
        if (so2 > so3) {
        document.getElementById('b1-result').innerHTML = `${so3} ${so2} ${so1} `;
        } else {
            document.getElementById('b1-result').innerHTML = `${so2} ${so3} ${so1} `;
        }
    } else if (so2>so1 && so1 > so3 ) { 
        if (so1 > so3) {
        document.getElementById('b1-result').innerHTML = ` ${so3} ${so1}   ${so2} `;
        } else {
        document.getElementById('b1-result').innerHTML = ` ${so1} ${so3} ${so2} `;
        }
    } else {
        if (so1 > so2 ) {
        document.getElementById('b1-result').innerHTML = `${so2} ${so1}    ${so3}`;
        } else {
        document.getElementById('b1-result').innerHTML = `${so1}  ${so2}   ${so3}  `;
        }
    }
})

/** bai 2
 * input: lay gia value who
 * progress xac dinh dung swith case
 * output xuat ra loi chao
 */


 document.getElementById('bai2-btn').addEventListener('click', function() {
    var who = document.getElementById('who').value;
    var ketQua = '';
    switch(who) {
        case 1: {
            ketQua = 'chao ban'
        }; break;
        case 2: {
            ketQua = 'chao emiu'
        }; break;
        default: {
            ketQua = 'chac cac ae';
        }
    }
    document.getElementById('bai2-result').innerText = ketQua;
})



/**
 * bai tap 3
 */
 document.getElementById('btn-bai3').onclick = function() {
    var so1 = document.getElementById('so1').value*1;
    var so2 = document.getElementById('so2').value*1;
    var so3 = document.getElementById('so3').value*1;
    ketQua = '';
    if(so1 % 2==0 && so2 %2==0 && so3%2==0) {
        ketQua = 'co 3 so chan';
    } else if(so1%2==0 && so2%2==0 || so2 %2==0 && so3%2==0||so1%2==0 && so3%2==0) {
        ketQua = 'co 2 so chan';
    } else if (so1%2==0 || so2%2==0 || so3%2==0) {
        ketQua = 'co 1 so chan' ;
    } else {
        ketQua = '3 so la so le'
    }
    document.getElementById('bai3-result').innerHTML = ketQua;
}