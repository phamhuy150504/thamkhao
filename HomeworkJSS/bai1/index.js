/**
 * input người dùng nhập vào 3 số nguyên
 * progress lấy value and ss use if else 
 * output sắp xếp theo thứ tự tăng dần
 */

document.getElementById('b1-btn').addEventListener('click', function() {
    var so1 = document.getElementById('so1').value*1;
    var so2 = document.getElementById('so2').value*1;
    var so3 = document.getElementById('so3').value*1;
    if (so1 > so2 && so1 > so3) {
        if (so2 > so3) {
        document.getElementById('b1-result').innerHTML = ` ${so1} ${so2} ${so3}`;
        } else {
            document.getElementById('b1-result').innerHTML = ` ${so1} ${so3}  ${so2}`;
        }
    } else if (so2>so1 && so1 > so3 ) { 
        if (so1 > so3) {
        document.getElementById('b1-result').innerHTML = ` ${so2} ${so1} ${so3}`;
        } else {
        document.getElementById('b1-result').innerHTML = ` ${so2} ${so3}  ${so1}`;
        }
    } else {
        if (so1 > so2 ) {
        document.getElementById('b1-result').innerHTML = `  ${so3}  ${so1} ${so2} `;
        } else {
        document.getElementById('b1-result').innerHTML = `  ${so3} ${so2}    ${so1} `;
        }
    }
})