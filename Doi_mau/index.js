var clickMe = document.getElementById('clickMe');
var reSet = document.getElementById('reSet');
var content = document.getElementById('content');
var body = document.getElementById('main')
body.style.backgroundColor = '#000000'


clickMe.addEventListener('click', function() {
    var maxVal = 0xFFFFFF;
    var randomNumber = Math.random() * maxVal;
    randomNumber = Math.floor(randomNumber);
    var randomColor = randomNumber.toString(16);

    body.style.backgroundColor = "#" + randomColor;
    body.style.transition = '1s'
    reSet.classList.remove('none')
    content.style.justifyContent = "space-around"
})
reSet.addEventListener('click', function () {
    body.style.backgroundColor = '#000000'
    body.style.transition = '1s'
    reSet.classList.add('none')
    content.style.justifyContent = "center"
})